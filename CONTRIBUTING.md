# Contributing Guidelines

Basiclly do what you want. I will just request changes.

**Thanks for your contribution**

### New URL

Add URL to `urls` file. Try out if the main script runs your url.

Would great if you sort the URLS with the command `sort urls -o urls`. But this isn't needed, it will also done automatically.

If a URL contains more than 200 MB, please create a issue before.

### Improve Source Code

Go to the source code and do your things and changes.

Fix [pylint](https://pylint.org/) issues and run [black](https://github.com/psf/black) as code formatter. Also not required.

### Submit Issue

Do it without restrictions. Every Issue is welcome. We close invalid issues, but of course also allow to reopen issue.