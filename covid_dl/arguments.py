"""
Parse Arguments passed to CLI
"""

import sys

args = sys.argv[1:]


def dicts():
    """
    Return Arguments that contains a pair (key, value)
    """
    parsed_args = []
    for arg in args:
        parsed_arg = []
        if arg.startswith("--"):
            splited_arg = arg.split("=")
            if len(splited_arg) > 1:
                parsed_arg.append(splited_arg[0].replace("--", "", 1))
                parsed_arg.append(splited_arg[1])
                parsed_args.append(parsed_arg)
    return parsed_args


def options():
    """
    Return options given like `-x` or `--test`
    """
    parsed_args = []
    for arg in args:
        splited_arg = arg.split("=")
        if len(splited_arg) == 1:
            if arg.startswith("-"):
                if arg.startswith("--"):
                    parsed_args.append(arg.replace("--", "", 1))
                else:
                    for letter in arg.replace("-", "", 1):
                        parsed_args.append(letter)
    return parsed_args


def standalone():
    """
    Returns a list of arguments not starting with `-`
    """

    parsed_args = []
    for arg in args:
        if not arg.startswith("-"):
            parsed_args.append(arg)
    return parsed_args
