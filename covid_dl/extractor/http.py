"""
All HTTP relevant extractor classes
"""

import os

# pylint: disable=E0401
from extractor.common import BaseExtractor

# pylint: disable=R0903
class HTTP(BaseExtractor):
    """
    Download simple URLs over HTTP/HTTPS
    """

    NAME = "http"

    def __init__(self, config):
        """
        Set Variables in class with Information from Config
        """

        BaseExtractor.__init__(self, config)

        if "input" in self.config:
            self.url = self.config["input"]
        else:
            self.url = self.config["url"]

        if "output_dir" in self.config:
            output_dir = self.config["output_dir"]
        else:
            output_dir = "data"

        self.file = (
            output_dir + "/" + self.url.replace("https://", "").replace("http://", "")
        )

    def extract(self):
        """
        Downloads the HTTP Request to Filename return by info function
        """

        os.system(f"curl '{self.url}' -o '{self.file}' --create-dirs --location")
