"""
Common Classes for Extractors
"""
# pylint: disable=R0903
class BaseExtractor:
    """
    Base Extractor for all extractors
    """

    def __init__(self, config):
        self.config = config
