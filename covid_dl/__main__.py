"""
Read config and run the specific extractors
"""

import inspect
import os
import toml
import extractors
import arguments

extractors = inspect.getmembers(extractors, inspect.isclass)

# Load configs
if len(arguments.standalone()) >= 1:
    all_configs = {}
    for arg in arguments.standalone():
        splited_arg = arg.split(":", 1)
        if len(splited_arg) == 1:
            all_configs[splited_arg[0]] = []
        else:
            all_configs[splited_arg[0]] = [{"input": splited_arg[1]}]
else:
    all_configs = toml.load(
        os.path.dirname(os.path.abspath(__file__)) + "/config/default.toml", _dict=dict
    )

# Run every extractor config
for extractor in extractors:
    extractor_class = extractor[1]
    COUNT = 0
    if extractor_class.NAME in all_configs:
        run_config = all_configs[extractor_class.NAME]
        for run in run_config:
            COUNT += 1
            print(f"({COUNT}/{len(run_config)} Download {extractor_class.NAME}")
            ext = extractor_class(run)
            ext.extract()
